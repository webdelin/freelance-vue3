import { createStore } from "vuex"
export const store = createStore({
    state() {
        return {
            items: []
        }
    },
    mutations: {
        SET_ITEMS(state) {
            state.items = JSON.parse(localStorage.getItem("items") || '[]')
        }
    },
    actions: {
        async EDITE_ITEMS({ state, commit }, data) {
            const items = state.items
            for (let i = 0; i < items.length; i++) {
                if (data.id == items[i].id) {
                    items[i].status = data.status;
                }
            }
            localStorage.setItem("items", JSON.stringify(items));
            commit('SET_ITEMS')
        }
    },
    getters: {
        GET_ITEMS(state) {
            return state.items
        },
        GET_ACTIVE(state) {
            return state.items.filter(i => i.status === 'active')
        }
    }
})