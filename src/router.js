import { createRouter, createWebHistory } from "vue-router"
import New from "./views/New"
import Tasks from "./views/Tasks"
import Task from "./views/Task"
export const router = createRouter({
    history: createWebHistory(),
    routes: [
        { path: '/', component: Tasks },
        { path: '/new', component: New },
        { path: '/:id', component: Task },
    ],
    linkActiveClass: 'active',
    linkExactActiveClass: 'active'
})